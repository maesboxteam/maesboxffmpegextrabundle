<?php

namespace Telemusic\CommonBundle\Libraries\Waveform;

use FFMpeg\Filters\Audio\AudioFilterInterface;
use FFMpeg\Format\AudioInterface;
use FFMpeg\Media\Audio;

class AudioWaveformFilter implements AudioFilterInterface
{
    /** @var integer */
    private $priority;
	
	/** @var array $options */
	protected $options;

    public function __construct(array $options = array(), $priority = 0)
    {
        $this->priority = $priority;
		$this->setOptions($this->checkOptions($options));
    }

    /**
     * {@inheritdoc}
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(Audio $audio, AudioInterface $format)
    {
        return array('-filter_complex', $this->processOptions($this->getOptions()), '-frames:v', 1);
    }
	
	protected function checkOptions(array $options)
	{
		$allowed_options = array("background-color", "width", "height", "mono", "expanded", "split");
		
		foreach($options as $name => $value)
		{
			if(!in_array($name, $allowed_options))
			{
				throw new \LogicException ($name." is not an allowed option");
			}
			
			
			if(in_array($name, $allowed_options))
			{
				//throw new \LogicException ($value." is not an allowed option");
			}
		}
		if(!isset($options['width']) && isset($options['height']))
		{
			$options['width'] = 600;
		}
		
		if(isset($options['width']) && !isset($options['height']))
		{
			$options['height'] = 240;
		}
		
		return $options;
	}
	
	protected function processOptions(array $options)
	{
		
		if(isset($options['expanded']) && $options['expanded'])
		{
			$values[] = 'compand';
		}
		
		if(isset($options['mono']) && $options['mono'])
		{
			$values[] = 'aformat=channel_layouts=mono';
		}
		
		$values[] = $this->processShowavespicOptions($options);
		$values[] = "colorkey=black";
				
		/** concat values to single string **/
		if(!empty($values))
		{
			
			foreach ($values as $key => $value)
			{
				if($key == 0)
				{
					$string = $value;
				}
				else
				{
					$string .= ",".$value;
				}
			}
		}
		
		return $string;
	}
	
	protected function processShowavespicOptions(array $options)
	{
		$string = "showwavespic";
		
		if(isset($options['width']) && isset($options['height']))
		{
			$showwavespic[] = "s=".$options['width']."x".$options['height'];
		}
		
		if(isset($options['split']) && $options['split'])
		{
			$showwavespic[] = 'split_channels=1';
		}
		
		/** concat values to single string **/
		if(!empty($showwavespic))
		{		
			$string .= "=";
			
			foreach ($showwavespic as $key => $value)
			{
				if($key == 0)
				{
					$string .= $value;
				}
				else
				{
					$string .= ":".$value;
				}
			}
		}
		
		return $string;
	}
	
	protected function convertHexColor($color)
	{
		$hex = str_replace("#", "", $color);

		if(strlen($hex) == 3) {
		   $r = hexdec(substr($hex,0,1).substr($hex,0,1));
		   $g = hexdec(substr($hex,1,1).substr($hex,1,1));
		   $b = hexdec(substr($hex,2,1).substr($hex,2,1));
		} else {
		   $r = hexdec(substr($hex,0,2));
		   $g = hexdec(substr($hex,2,2));
		   $b = hexdec(substr($hex,4,2));
		}
		$rgb = "rr=$r/255:gg=$g/255:bb=$b/255"; //array($r, $g, $b);
		//return implode(",", $rgb); // returns the rgb values separated by commas
		return $rgb;
	}
	
	/**
	 * @param array $options
	 * @return \Telemusic\CommonBundle\Libraries\Waveform\AudioWaveformFilter
	 */
	public function setOptions(array $options = array())
	{
		$this->options = $options;
		return $this;
	}
	
	/**
	 * @return array
	 */
	public function getOptions()
	{
		return $this->options;
	}
}