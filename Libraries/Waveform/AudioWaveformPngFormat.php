<?php

namespace Telemusic\CommonBundle\Libraries\Waveform;

use FFMpeg\Format\AudioInterface;

class AudioWaveformPngFormat implements AudioInterface
{
	public function getAudioChannels()
	{
		return null;
	}

	public function getAudioCodec()
	{
		return null;
	}

	public function getAudioKiloBitrate()
	{
		return null;
	}

	public function getAvailableAudioCodecs()
	{
		return null;
	}

	public function getExtraParams()
	{
		return array();
	}

	public function getPasses()
	{
		return 1;
	}

}