<?php

namespace Maesbox\FfmpegExtraBundle\Service;

use FFMpeg\FFMpeg;


use FFMpeg\Media\Audio;

use Maesbox\FfmpegExtraBundle\Libraries\Waveform\AudioWaveformFilter;
use Maesbox\FfmpegExtraBundle\Libraries\Waveform\AudioWaveformPngFormat;

class WaveformGenerator
{

	/**
	 * @var FFMpeg $ffmpeg
	 */
	protected $ffmpeg;
	
	/**
	 * @var array $options
	 */
	protected $options = array();
	
	/**
	 * @var Audio 
	 */
	protected $audio;
	
	protected $color;
	
	public function __construct(FFMpeg $ffmpeg, array $options = array(), $color = "#CCCC00")
	{
		$this->setFFMpeg($ffmpeg)->setOptions($options)->setColor($color);
		
	}
	
	public function open($file)
	{
		$this->setAudio($this->getFFMpeg()->open($file));
		
	}
	
	/**
	 * @param type $file
	 * @return type
	 * @throws \LogicException
	 */
	public function generate($file)
	{
		$this->getAudio()->addFilter(new AudioWaveformFilter($this->getOptions()));
		
		switch(pathinfo($file,PATHINFO_EXTENSION))
		{
			case "png":
				$format = new AudioWaveformPngFormat();
				$this->getAudio()->save($format,$file);
				$color = $this->getColor();
				$im = imagecreatefrompng($file);
				imagefilter($im, IMG_FILTER_COLORIZE, $color[0]-255, $color[1] , $color[2]);
				$back = imagecolorresolve($im, 0, 0, 0);
				imagecolortransparent($im, $back);
				imagepng($im, $file);
				imagedestroy($im);
				break;
			case "jpg":
			case "jpeg":
			case "bmp":
			default:
				throw new \LogicException ("image format not supported");
				break;
		}
		
		return $file;
	}


	/**
	 * @param FFMpeg $ffmpeg
	 * @return \Telemusic\CommonBundle\Service\WaveformGenerator
	 */
	public function setFFMpeg(FFMpeg $ffmpeg)
	{
		$this->ffmpeg = $ffmpeg;
		return $this;
	}
	
	/**
	 * @return FFMpeg
	 */
	public function getFFMpeg()
	{
		return $this->ffmpeg;
	}
	
	/**
	 * @param array $options
	 * @return \Telemusic\CommonBundle\Service\WaveformGenerator
	 */
	public function setOptions(array $options)
	{
		$this->options = $options;
		return $this;
	}
	
	/**
	 * @return array
	 */
	public function getOptions()
	{
		return $this->options;
	}
	
	/**
	 * @return Audio
	 */
	public function getAudio()
	{
		return $this->audio;
	}
	
	/**
	 * @param Audio $audio
	 * @return \Telemusic\CommonBundle\Service\WaveformGenerator
	 */
	public function setAudio(Audio $audio)
	{
		$this->audio = $audio;
		return $this;
	}
	
	/**
	 * @param int $width
	 * @return \Telemusic\CommonBundle\Service\WaveformGenerator
	 */
	public function setWidth($width)
	{
		$this->options['width'] = $width;
		return $this;
	}
	
	/**
	 * @param int $height
	 * @return \Telemusic\CommonBundle\Service\WaveformGenerator
	 */
	public function setHeight($height)
	{
		$this->options['height'] = $height;
		return $this;
	}
	
	/**
	 * @param int $width
	 * @param int $height
	 * @return \Telemusic\CommonBundle\Service\WaveformGenerator
	 */
	public function setFormat($width, $height)
	{
		$this->setWidth($width)->setHeight($height);
		return $this;
	}
	
	/**
	 * @param string $hex
	 * @return \Telemusic\CommonBundle\Service\WaveformGenerator
	 */
	public function setColor($hex)
	{
		$this->color = sscanf(str_replace("#","",$hex),"%2x%2x%2x");
		return $this;
	}
	
	/**
	 * 
	 * @return array
	 */
	public function getColor()
	{
		return $this->color;
	}
}